import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AllUsersComponent } from './Components/Users/all-users/all-users.component';
import { UserslistComponent } from './Components/Users/userslist/userslist.component';
import { UpdatedataComponent } from './Components/Users/updatedata/updatedata.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from "@angular/common/http";
import {MatTableModule} from '@angular/material/table';
import { MatButtonModule} from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AddItemComponent } from './Components/add-item/add-item.component';
import { DataBindingComponent } from './Components/data-binding/data-binding.component';
import { SignUpformComponent } from './Components/sign-upform/sign-upform.component';
import { SuccessSignupComponent } from './Components/success-signup/success-signup.component';

const appRoutes: Routes = [
  { path: 'Users', component: AllUsersComponent },
  { path: 'listusers', component: UserslistComponent },
  { path: 'update/:uID', component: UpdatedataComponent },
];
@NgModule({
  declarations: [
    AppComponent,
    AllUsersComponent,
    UserslistComponent,
    SignUpformComponent,
    AddItemComponent,
    DataBindingComponent,
    UpdatedataComponent,
    SuccessSignupComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MatButtonModule,
    MatTableModule,
    MatToolbarModule,
    BrowserAnimationsModule,
    AppRoutingModule,    
    ReactiveFormsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    )

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UserData } from '../Model/users';
import { GetuserDataService } from '../Service/getuser-data.service';
import { HttpClient } from '@angular/common/http';
import { Router } from "@angular/router";

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.scss']
})
export class AllUsersComponent implements OnInit {
  selectedFile: File = null;
  UserForm: FormGroup;
  fileData: File = null;
  reader;

  constructor(private fb: FormBuilder, private getlist: GetuserDataService, private http: HttpClient, private router:Router) { }

  ngOnInit() {
    // this.UserForm = this.fb.group({
    //   userData: this.fb.group(new UserData())
    // });
    this.UserForm = this.fb.group({
      fname: ["", Validators.required],
      lname: ["", Validators.required],
      gender: [""],
      age: ["", Validators.required],
      proffession: ["", Validators.required],
      img: ["", Validators.required],
      filedata: ["", Validators.required]
    });
  }

  onSubmit() {
    // const formData = new FormData();
    // formData.append('file', this.selectedFile, this.selectedFile.name);
    const result = Object.assign({}, this.UserForm.value);
    const newResult: UserData = result;
    this.getlist.addUsers(newResult);
    console.log(newResult);
    this.UserForm.reset();
    this.router.navigate(['/listusers']);

  }
  onSelectedChange(event) {
    if (event.target.files.length > 0) {
      this.reader = new FileReader();
      this.reader.readAsDataURL(event.target.files[0]);
      this.reader.onload = (event) => { 
        this.UserForm.patchValue({
          filedata: event.target.result
        });
      };
    }
  }

}

import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { UserData } from '../Model/users';
@Injectable({
  providedIn: 'root'
})
export class GetuserDataService {

  userChanged = new Subject<any>();
  userList: UserData[] = [];
  currentuser: any;
  constructor() { }
  addUsers(data) {
    this.userList.push(data);
    this.userChanged.next(this.userList.slice());
  }

  getUsers() {
    return this.userList.slice();
  }

  updateList(index) {
    this.currentuser = this.userList[index];
    return this.currentuser;

  }
  updateRecord(id:number,updatedData: UserData){
 this.userList[id]=updatedData;
   }
}

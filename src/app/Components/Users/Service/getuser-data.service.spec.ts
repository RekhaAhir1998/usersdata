import { TestBed } from '@angular/core/testing';

import { GetuserDataService } from './getuser-data.service';

describe('GetuserDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetuserDataService = TestBed.get(GetuserDataService);
    expect(service).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { GetuserDataService } from '../Service/getuser-data.service';

@Component({
  selector: 'app-userslist',
  templateUrl: './userslist.component.html',
  styleUrls: ['./userslist.component.scss']
})
export class UserslistComponent implements OnInit {
  userlist: any[] = [];
  currentuser: any;
  constructor(public getdata: GetuserDataService) { }

  ngOnInit() {
    this.getdata.userChanged.subscribe(res => {
      this.userlist = res;
    });
    this.userlist = this.getdata.getUsers();
  }
  selectedUser(index) {
    this.currentuser = this.userlist[index];
    this.getdata.updateList(index);
  }


}

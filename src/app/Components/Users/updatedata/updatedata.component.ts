import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UserData } from '../Model/users';
import { Router } from "@angular/router";

import { GetuserDataService } from '../Service/getuser-data.service';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-updatedata',
  templateUrl: './updatedata.component.html',
  styleUrls: ['./updatedata.component.scss']
})
export class UpdatedataComponent implements OnInit {

  user: any;
  UpdateForm: FormGroup;
  userId: number = 0;
  constructor(private userservices: GetuserDataService, private fb: FormBuilder, private actRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.userId = parseInt(this.actRoute.snapshot.params['uID']);
    console.log("id is", this.userId);
    this.updatelist(this.userId);
  }
  
  updatelist(index) {
    this.user = this.userservices.updateList(index);
    if (this.user) {
      this.UpdateForm = this.fb.group({
        userData: this.fb.group(this.user)
      });
    }
  }
  onSubmit() {
    const result = Object.assign({}, this.UpdateForm.value);
    const newResult: UserData = result.userData;
    this.userservices.updateRecord(this.userId, newResult);
    this.router.navigate(['/listusers']);
  }
}

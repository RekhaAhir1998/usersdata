import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {

  addItemForm: FormGroup;
  itemName: string = "";
  itemCount: number;
  isDisabled: boolean = true;
  itemList: Array<{ itemName: string, itemCount: number }> = [];
  constructor(private fb: FormBuilder) {

    this.addItemForm = this.fb.group({
      item: '',
      count: 0
    })
  }

  ngOnInit() {

  }
  onSubmit() {
    this.itemName = this.addItemForm.get('item').value;
    this.itemCount = this.addItemForm.get('count').value;

    let itemObj = {
      itemName: this.itemName,
      itemCount: this.itemCount
    }
    console.log(this.itemList)
    this.itemList.push(itemObj);
    this.addItemForm.reset();
  }
  showButton(data) {
    if (data.target.value !== "") {
      this.isDisabled = false;
    }
    else {
      this.isDisabled = true;
    }
  }


}

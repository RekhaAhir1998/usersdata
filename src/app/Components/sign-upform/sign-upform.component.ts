import { Component, OnInit } from '@angular/core';

import { GetdataService } from '../../Services/getdata.service';

import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { PersonalData } from '../../Model/dataModel';
@Component({
  selector: 'app-sign-upform',
  templateUrl: './sign-upform.component.html',
  styleUrls: ['./sign-upform.component.scss']
})
export class SignUpformComponent implements OnInit {
  loginForm: FormGroup;
  name: '';
  passWord1: '';
  stateList = ['Andhra Pradesh', 'Arunachal Pradesh', 'Assam', 'Bihar', 'Chhattisgarh', 'Goa', 'Gujarat', 'Haryana', 'Himachal Pradesh', 'Jharkhand', 'Karnataka', 'Kerala', 'Madhya Pradesh', 'Maharashtra', 'Manipur', 'Meghalaya', 'Mizoram', 'Nagaland', 'Odisha', 'Punjab', 'Rajasthan', 'Sikkim', 'Tamil Nadu', 'Telangana', 'Tripura', 'Uttar Pradesh', 'Uttarakhand', 'West Bengal'];
  constructor(private fb: FormBuilder, private getdataService: GetdataService) {
    // this.loginForm = this.fb.group({
    //   username: '',
    //   password: ''
    // })
   // this.loginForm = this.createFormGroup();
  }


  ngOnInit() {
    this.loginForm = this.fb.group({
      personalData: this.fb.group(new PersonalData())
    })
  }
  // createFormGroup() {
  //   return new FormGroup({
  //     personalData: new FormGroup({
  //       fname: new FormControl(),
  //       lname: new FormControl(),
  //       gender: new FormControl(),
  //       email: new FormControl(),
  //       password: new FormControl(),
  //       state: new FormControl()


  //     })

  //   });
  // }
  onSubmit() {
    const result: PersonalData = Object.assign({}, this.loginForm.value);
  //  console.log(result);
    // this.passWord1 = this.loginForm.get('password').value;
    // this.name = this.loginForm.get('username').value;
    // console.log(this.passWord1, this.name);
    // // const {
    //   username,
    //   password
    // } = this.loginForm.value;

    // const dataval = {
    //   name: this.name,
    //   passWord1: this.passWord1
    // };
    this.getdataService.add(result);


    this.loginForm.reset();
  }
}

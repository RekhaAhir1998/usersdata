import { Component, OnInit } from '@angular/core';

import { GetdataService } from '../../Services/getdata.service';
import { PersonalData } from 'src/app/Model/dataModel';
@Component({
  selector: 'app-success-signup',
  templateUrl: './success-signup.component.html',
  styleUrls: ['./success-signup.component.scss']

})
export class SuccessSignupComponent implements OnInit {
  listData: any;
  currentList = [];


  constructor(public getdataService: GetdataService) { }

  ngOnInit() {
    this.currentList = this.getdataService.showList();
    
  }
}
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  templateUrl: './data-binding.component.html',
  styleUrls: ['./data-binding.component.scss']
})
export class DataBindingComponent implements OnInit {
  stringInt = 'String XYZ';
  namehere;
  isShow = false;
  showHide = false;
  array1: any[] = [];
  currentStyles = {};
  constructor() { }

  ngOnInit() {
    // this.array1=["one","two","three"];

    this.array1 = [{
      name: 'abc',
      value: 0
    }, {
      name: 'pqr',
      value: 1
    }, {
      name: 'mno',
      value: 2
    }];
  }
  testFunction() {
    this.isShow = !this.isShow;
  }
  showOnClick() {
    this.showHide = !this.showHide;
    this.currentStyles = {
      'font-style': this.showHide ? 'italic' : 'normal',
      'font-weight': !this.showHide ? 'bold' : 'normal',
      'font-size': this.showHide ? '24px' : '12px'
    };
  }

}
